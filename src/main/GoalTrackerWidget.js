// Suivi du jour pour un objectif
// ==============================

import Add from '@material-ui/icons/Add'
import Fab from '@material-ui/core/Fab'
import ThumbUp from '@material-ui/icons/ThumbUp'
import Typography from '@material-ui/core/Typography'

import classes from './TrackerScreen.module.css'
import { func, GoalPropType, nonNegativeInteger } from '../shared/prop-types'
import Gauge from '../shared/Gauge'

// Section de l'écran principal, dédiée à un objectif.  Fournit notamment le
// descriptif de l'objectif et l’éventuel bouton de progression.

export default function GoalTrackerWidget({
  // La déstructuration en force !
  goal,
  goal: { name, units, target },
  onProgress,
  progress,
}) {
  // La beauté d'un ternaire multi-lignes…
  const adderComponent =
    target > progress ? (
      <Fab color='secondary' onClick={() => onProgress?.(goal)} size='small'>
        <Add data-testid='in-progress' />
      </Fab>
    ) : (
      <Fab disabled size='small'>
        <ThumbUp data-testid='completed' />
      </Fab>
    )

  return (
    <div className={classes.goal}>
      <div className={classes.summary}>
        <Typography variant='h6' component='h2'>
          {name}
        </Typography>
        <Gauge value={progress} max={target} />
        <Typography component='small'>
          {`${progress} ${units} sur ${target}`}
        </Typography>
      </div>
      <div className={classes.cta}>{adderComponent}</div>
    </div>
  )
}

GoalTrackerWidget.propTypes = {
  goal: GoalPropType.isRequired,
  progress: nonNegativeInteger.isRequired,
  onProgress: func,
}
